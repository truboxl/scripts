@echo off

:: For Windows 7, no longer useful for Windows 10
:: Change your Access Point name and password here
:: Do not include spaces commas slashes underscores etc
set apname=hotspot_nomap
set passwd=hotspot

cls
title Requesting Admin Control - Please approve
rem -- BatchGotAdmin
:-------------------------------------
rem -- Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

rem -- If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
:--------------------------------------

title Preparing Ad-hoc connection
echo Begin Ad-hoc connection in seconds...
echo.
timeout /t 5
cls

rem -- In progress, need to set up an error here if WiFi adapter not support ad-hoc
rem -- echo Checking WiFi adapter functionality...
rem -- echo.
rem -- netsh wlan show drivers

echo Setting up ad-hoc connection...
echo.
echo Your current ad-hoc connection is named %apname%
echo.
echo and the password is %passwd%
echo.
echo You can change it later by open up this script with Notepad or Wordpad.
echo.
netsh wlan set hostednetwork mode=allow ssid=%apname% key=%passwd%
echo.
pause
cls
echo Starting up ad-hoc connection...
echo.
netsh wlan start hostednetwork
echo.
timeout /t 5

cls
title Ad-hoc connection now Online!
echo Ad-hoc session in progress...
echo.
echo You can now share files under Public folders.
echo Refer the Internet on what other things you can do...
echo.
echo When you are done, press any key to stop.
echo.
echo.
echo.
pause

cls
echo Stopping ad-hoc...
echo.
netsh wlan stop hostednetwork
title Ad-hoc connection now Offline.
echo.
echo Ad-hoc connection now Offline.
echo.
echo Thanks for using this script. You can now close the window.
echo.
timeout /t 10

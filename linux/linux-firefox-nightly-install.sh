#!/bin/sh
set -e
# Mozilla Firefox Nightly Linux Installation Script V2

# References
# https://archive.mozilla.org/pub/firefox/releases/latest/README.txt
# https://www.mozilla.org/en-US/firefox/nightly/all/

########## GLOBAL VARIABLES ##########
# TODO allow user input argument to change during executing script
# TODO also add help lines
DEFAULT_MOZILLA_DIR="${HOME}/.mozilla"
DEFAULT_INSTALL_DIR="${DEFAULT_MOZILLA_DIR}/nightly"
DEFAULT_PROFILE_DIR="${DEFAULT_MOZILLA_DIR}/firefox"

DEFAULT_DESKTOP_FILE="firefox-nightly.desktop"

TEMPORARY_CACHE_DIR="${TMPDIR:-/tmp}/mozilla"
TEMPORARY_DOWNLOAD_FILE="firefox-nightly.tar.bz2"

XDG_APPS_DIR="${HOME}/.local/share/applications"
XDG_MIME_DIR="${HOME}/.local/share/mime"

OSYS="${OSYS:-linux64}"
LANG=en-US
VER=firefox-nightly-latest-ssl
URL="https://download.mozilla.org/?product=${VER}&os=${OSYS}&lang=${LANG}"
#echo "Download URL is $URL"

########## DOWNLOAD ##########
# TODO add argument to accept offline install if tar exist, skips download
mkdir -p "$TEMPORARY_CACHE_DIR"

if [ -e "${TEMPORARY_CACHE_DIR}/${TEMPORARY_DOWNLOAD_FILE}" ]; then
    echo "Previous downloaded ${TEMPORARY_DOWNLOAD_FILE} found"
    echo 'Trying to resume download ...'
    curl -L# "$URL" -o "${TEMPORARY_CACHE_DIR}/${TEMPORARY_DOWNLOAD_FILE}" -C - || \
        echo -e "\n\nDownloaded ${TEMPORARY_DOWNLOAD_FILE} cannot be resumed. Continuing anyway..."
else
    echo "Downloading Mozilla Firefox Nightly as ${TEMPORARY_DOWNLOAD_FILE} ..."
    curl -L# "$URL" -o "${TEMPORARY_CACHE_DIR}/${TEMPORARY_DOWNLOAD_FILE}"
fi
echo


########## INSTALLATION ##########
# TODO add error handling for tar
echo "Extracting ${TEMPORARY_CACHE_DIR}/${TEMPORARY_DOWNLOAD_FILE} to ${TEMPORARY_CACHE_DIR} ..."
tar -xf "${TEMPORARY_CACHE_DIR}/${TEMPORARY_DOWNLOAD_FILE}" -C "$TEMPORARY_CACHE_DIR"
echo

echo "Removing previous installation ${DEFAULT_INSTALL_DIR} ..."
rm -fr "$DEFAULT_INSTALL_DIR"
echo

echo "Copying files from ${TEMPORARY_CACHE_DIR}/firefox to ${DEFAULT_INSTALL_DIR} ..."
mkdir -p "$DEFAULT_MOZILLA_DIR"
cp -fr "${TEMPORARY_CACHE_DIR}/firefox" "$DEFAULT_INSTALL_DIR"
echo


########## CONFIGURATION ##########
# TODO need to follow Windows version
# TODO add i10n entries as well?
echo 'Creating desktop shortcut file ...'
tee "${TEMPORARY_CACHE_DIR}/${DEFAULT_DESKTOP_FILE}" >/dev/null << EOF
[Desktop Entry]
Version=1.0
Name=Firefox Nightly
GenericName=Web Browser
Comment=Browse the Web
Exec=${DEFAULT_INSTALL_DIR}/firefox %u
Icon=${DEFAULT_INSTALL_DIR}/browser/chrome/icons/default/default128.png
Terminal=false
Type=Application
MimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;message/rfc822;x-scheme-handler/mailto;image/webp;
StartupNotify=true
Categories=Network;WebBrowser;Email;
Keywords=web;browser;internet;
Actions=new-window;new-private-window;profile-manager;

X-Desktop-File-Install-Version=0.23

[Desktop Action new-window]
Name=Open a New Window
Exec=${DEFAULT_INSTALL_DIR}/firefox --new-window %u

[Desktop Action new-private-window]
Name=Open a New Private Window
Exec=${DEFAULT_INSTALL_DIR}/firefox --private-window %u

[Desktop Action profile-manager]
Name=Profile Manager
Exec=${DEFAULT_INSTALL_DIR}/firefox --ProfileManager %u
EOF

cp -fv "${TEMPORARY_CACHE_DIR}/${DEFAULT_DESKTOP_FILE}" "${XDG_APPS_DIR}/${DEFAULT_DESKTOP_FILE}"
echo

echo 'Symbolic linking shortcut file to desktop ...'
mkdir -p "${HOME}/Desktop"
rm -f "${HOME}/Desktop/${DEFAULT_DESKTOP_FILE}"
ln -sv "${XDG_APPS_DIR}/${DEFAULT_DESKTOP_FILE}" "${HOME}/Desktop/${DEFAULT_DESKTOP_FILE}"
echo

echo 'Updating desktop database cache ...'
update-desktop-database "$XDG_APPS_DIR"
echo

echo 'Update mime database cache ...'
mkdir -p "${XDG_MIME_DIR}/packages"
update-mime-database "$XDG_MIME_DIR"
echo


########## CLEANUP ##########
echo 'Cleaning up temporary folders and files ...'
rm -fr "$TEMPORARY_CACHE_DIR"
echo


########## COMPLETE ##########
echo "Mozilla Firefox Nightly has been successfully installed!
Launch the command below to begin:

${DEFAULT_INSTALL_DIR}/firefox

OR

${XDG_APPS_DIR}/${DEFAULT_DESKTOP_FILE}"

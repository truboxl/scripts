#!/bin/sh
wineserver -k
rm -frv ~/.wine
find ~/.local/share \( -name '*wine*' ! -path "*Steam*" ! -path "*steam*" \) -exec rm -frv {} \;

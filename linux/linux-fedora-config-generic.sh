#!/bin/sh
set -e

SCRIPT="$(echo $0 | sed -E -e "s|$(dirname $0)+/||")"

if [ "$UID" = 0 ]; then
    echo "Running $SCRIPT as root!"
    unset SU
    echo
else
    echo "Not running $SCRIPT as root!"
    echo 'Prepending certain commands with sudo'
    SU=sudo
    echo
fi

##### HIGH PRIORITY #####

if [ -e /proc/sys/kernel/sysrq ]; then
    echo 'Enabling all magic SysRq key at current runtime'
    echo '1' | $SU tee '/proc/sys/kernel/sysrq' >/dev/null
    echo
    echo 'Current magic SysRq value is:'
    tee </proc/sys/kernel/sysrq
    echo
    echo 'Enabling all magic SysRq key functions at next reboot'
    echo 'kernel.sysrq = 1' | $SU tee -a /etc/sysctl.d/90-sysrq.conf >/dev/null
    echo
fi

if [ -f /etc/dnf/dnf.conf ]; then
    echo 'Configuring DNF'
    $SU tee -a '/etc/dnf/dnf.conf' << EOF >/dev/null
deltarpm_percentage=99
keepcache=True
metadata_expire=-1
metadata_timer_sync=0
EOF
    echo
fi

if [ -f /etc/yum.repos.d/fedora-modular.repo ]; then
    echo 'Disabling Fedora Modularity repos'
    $SU dnf config-manager --disable fedora-modular || \
    $SU sed -e 's/^enabled=1$/enabled=0/' -i /etc/yum.repos.d/fedora-modular.repo
    $SU dnf config-manager --disable updates-modular || \
    $SU sed -e 's/^enabled=1$/enabled=0/' -i /etc/yum.repos.d/fedora-updates-modular.repo
    echo
fi

##### MEDIUM PRIORITY #####

if [ "$(rpm -E %fedora)" -lt 32 ]; then
    echo 'Enabling fstrim'
    $SU systemctl enable fstrim.timer # Fedora not enabling for SSD until F32
    echo
fi

if [ -f /usr/lib/systemd/zram-generator.conf ]; then
    echo 'Configuring zram'
    if [ ! -f /etc/systemd/zram-generator.conf ]; then
        cpus="$(nproc)"
        count=0
        while [ "$count" -lt "$cpus" ]; do
            echo "Adding entry zram${count} to /etc/system/zram-generator.conf"
            echo "[zram${count}]" | $SU tee -a /etc/systemd/zram-generator.conf >/dev/null
            count=$(($count+1))
        done
    fi
    echo
fi

echo 'Configuring PowerTOP and Tuned'
$SU dnf install -y tuned-utils
echo
# powertop2tuned will check PowerTOP and create settings for use in Tuned
# However it will enable autosuspend for mouse unfortunately
# Better remove mouse and external devices before running powertop2tuned
sudo powertop2tuned -n -e optimized
# TODO haven't found a clean way to only exclude mouse from blacklist
# powertop2tuned should disable autosuspend for mouse
# But it is still not working, have to do it ourselves
# NOTE this exclude all usb devices from autosuspend including mouse
$SU sed -e 's|/sys/bus/usb|#/sys/bus/usb|g' -i /etc/tuned/optimized/tuned.conf
$SU tuned-adm profile optimized
$SU systemctl enable tuned
echo

##### LOW PRIORITY #####

if [ "$(echo $XDG_CURRENT_DESKTOP | grep -i gnome)" ]; then
    echo 'Configuring GNOME'
    gsettings set org.gnome.FileRoller.General compression-level 'maximum'
    gsettings set org.gnome.gnome-system-monitor solaris-mode 'false'
    gsettings set org.gnome.mutter experimental-features "['rt-scheduler']"
    echo
    $SU dnf install -y gnome-tweaks dconf-editor
    echo
fi

echo "Done running $SCRIPT"

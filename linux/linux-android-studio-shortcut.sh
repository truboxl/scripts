#!/bin/sh
set -e

DEFAULT_INSTALL_DIR=~/Android/android-studio
XDG_APPS_DIR=~/.local/share/applications
XDG_MIME_DIR=~/.local/share/mime

mkdir -p "${XDG_APPS_DIR}"
mkdir -p "${XDG_MIME_DIR}"

echo 'Creating desktop shortcut file'
tee ./android-studio.desktop &>/dev/null << EOF
[Desktop Entry]
Version=1.0
Name=Android Studio
Exec=${DEFAULT_INSTALL_DIR}/bin/studio.sh
Icon=${DEFAULT_INSTALL_DIR}/bin/studio.png
Terminal=true
Type=Application
StartupNotify=true
Categories=
Keywords=

X-Desktop-File-Install-Version=0.23

EOF
mv -v ./android-studio.desktop ${XDG_APPS_DIR}
echo

echo 'Symbolic linking shortcut file to desktop'
mkdir -p ~/Desktop
rm -f ~/Desktop/android-studio.desktop
ln -sv ${XDG_APPS_DIR}/android-studio.desktop ~/Desktop/android-studio.desktop
echo

echo 'Updating desktop database cache'
update-desktop-database ${XDG_APPS_DIR}
echo

echo 'Updating mime database cache'
update-mime-database ${XDG_MIME_DIR}
echo

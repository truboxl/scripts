#!/bin/sh
set -e

##### HIGH PRIORITY #####

sh $PWD/linux-fedora-config-generic.sh

##### ADDON #####

SCRIPT="$(echo $0 | sed -E -e "s|$(dirname $0)+/||")"

if [ "$UID" = 0 ]; then
    echo "Running $SCRIPT as root!"
    unset SU
    echo
else
    echo "Not running $SCRIPT as root!"
    echo 'Prepending certain commands with sudo'
    SU=sudo
    echo
fi

#echo 'Enabling power off HDD on shutdown'
#$SU tee /usr/lib/systemd/system-shutdown/poweroffhdd.shutdown << EOF >/dev/null
##!/usr/bin/sh
#udisksctl power-off
#EOF
#echo

echo 'Enabling RPMFusion repo' # https://rpmfusion.org/Configuration
$SU dnf install -y \
"https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" \
"https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"
echo
echo 'Enabling RPMFusion Appstream'
$SU dnf groupupdate -y core
echo

echo 'Installing non-free multimedia component'
$SU dnf groupupdate -y multimedia --setop='install_weak_deps=False' \
                    --exclude=PackageKit-gstreamer-plugin
$SU dnf groupupdate -y sound-and-video
$SU dnf install -y gstreamer1-vaapi libva-utils libva-intel-driver \
                   ffmpeg-libs compat-ffmpeg28
echo

echo 'Installing packages'
$SU dnf install -y @virtualization p7zip{,-plugins} vim-enhanced \
                   clinfo pocl \
                   java-11-openjdk-devel \
                   clang codeblocks codeblocks-contrib
echo

echo 'Installing LibreOffice'
$SU dnf install -y libreoffice-{writer,calc,impress,math} wine-fonts
echo

echo 'Installing Steam'
$SU dnf install -y steam
echo
$SU setsebool -P selinuxuser_execheap 1
echo

echo 'Installing Visual Studio Code'
$SU rpm --import 'https://packages.microsoft.com/keys/microsoft.asc'
$SU tee '/etc/yum.repos.d/vscode.repo' << EOF >/dev/null
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF
echo
$SU dnf install -y code
echo

##### THINKPAD #####

echo 'Configuring ThinkPad Power Management'
$SU dnf install -y "https://repo.linrunner.de/fedora/tlp/repos/releases/tlp-release.fc$(rpm -E %fedora).noarch.rpm"
$SU dnf install -y tlp akmod-acpi_call lm_sensors thinkfan
$SU systemctl enable tlp
$SU systemctl enable thinkfan # ThinkPad
$SU sed -e 's|^(.*|#&|g' -i /etc/thinkfan.conf
$SU tee -a /etc/thinkfan.conf << EOF > /dev/null
(0,	0,	45)
(1,	45,	50)
(2,	50,	55)
(3,	55,	60)
(4,	60,	65)
(5,	65,	70)
(6,	70,	75)
(7,	75,	32767)
EOF
echo

echo "Done running $SCRIPT"

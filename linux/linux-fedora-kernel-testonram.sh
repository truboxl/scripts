#!/bin/sh

# Q: Why run kernel test on RAM?
# A: Faster, does away the need to fix permission for the files on persistent disks

if [ -z "$@" ] || [ "$@" = '-h' ] || [ "$@" = '--help' ]; then
    echo './linux-fedora-kernel-testonram.sh [username] [password]'
    echo 'Test Fedora kernel on RAM. Upload result to FAS.'
    exit
fi

# install dependencies
sudo dnf install -y git python3-fedora make libtirpc-devel gcc #clang

# clone / update kernel test repo
if [ -d ~/kernel-tests ]; then
    cd ~/kernel-tests
    git reset --hard
    git clean -fxd
    git pull origin master
else
    git clone --depth 1 'https://pagure.io/kernel-tests' ~/kernel-tests
fi

# create temporary folder on RAM
cd "$(mktemp -d)"

# copy kernel test folder to RAM
cp -rv ~/kernel-tests "${PWD}"

# change directory to kernel test folder insider temporary folder on RAM
cd ./kernel-tests

# copy example configuration file to kernel test folder on RAM
cp -fv "${PWD}"/config.example "${PWD}"/.config

# edit .config
sed -e 's|^submit=none|# &|' -i "${PWD}"/.config
sed -e 's|^# submit=authenticated|submit=authenticated|' -i "${PWD}"/.config
sed -e "s|^#username=.*|username='$1'|" -i "${PWD}"/.config
sed -e "s|^#password=.*|password='$2'|" -i "${PWD}"/.config # highly insecure

# skip problematic tests
rm -frv ./default/{cachedrop,insert_leap_second}/runtest.sh
	# fails if you are running other jobs at the same time
	echo -e '#!/bin/sh\nexit 3' >./default/cachedrop/runtest.sh
	# cant restore time if your network blocks ntp
	echo -e '#!/bin/sh\nexit 3' >./default/insert_leap_second/runtest.sh
	# fails on old Intel CPU because meltdown bug, seems like disabled on upstream
	#echo -e '#!/bin/sh\nexit 3' >./default/paxtest/runtest.sh
chmod +x ./default/{cachedrop,insert_leap_second}/runtest.sh

# feeling experimental?
#export CC=clang

# tests took way too long if not optimise
export CFLAGS="-O3 -march=native"

# run performance test then default test on RAM
tee ./kernel-tests.sh << EOF >/dev/null
#!/bin/sh
echo 'performance' | tee /sys/devices/system/cpu/cpufreq/policy*/scaling_governor >/dev/null
./runtests.sh -t performance
rm -fr /usr/tmp/lmbench
./runtests.sh
echo 'powersave' | tee /sys/devices/system/cpu/cpufreq/policy*/scaling_governor >/dev/null
exit
EOF
chmod +x ./kernel-tests.sh
time sudo ./kernel-tests.sh

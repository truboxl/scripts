#!/bin/bash
# .bash_profile

# rules:
# use alias for tasks that does not need references
# use function for tasks that need updated references
# use environment variables for reuse

# comments:
# use chrt then taskset for lesser system calls

# very important not to mess up these lines
alias edit_bash_profile='editor $HOME/.bash_profile'
alias reload_bash_profile='. $HOME/.bash_profile'
alias shellcheck_bash_profile='shellcheck $HOME/.bash_profile'
alias show_bash_profile_fn='grep \(\) $HOME/.bash_profile'

alias edit_vimrc='editor $HOME/.vimrc'
alias lsa='ls -a'
alias lsA='ls -A'
alias show_du_in_this_folder='du -hs .[^.]* * | sort -h'
alias ncdu_com_termux='ncdu /data/data/com.termux'
alias ncdu_storage_emulated_0='ncdu /storage/emulated/0'
alias ncdu2_com_termux='ncdu2 /data/data/com.termux'
alias ncdu2_storage_emulated_0='ncdu2 /storage/emulated/0'

# bin path
if [ -d "${HOME}/bin" ]; then export PATH="${HOME}/bin:${PATH}"; fi

# scheduling
alias taskset_pid='taskset -ap'
alias use_all_cores='taskset_pid ff'
alias use_little_cores='taskset_pid 3f'
alias use_big_cores='taskset_pid c0'
alias chrt_pid_idle='chrt -ip 0'
alias super_nice='chrt -i 0 nice -n 39'

# system monitoring
alias psax='ps ax -H -o pid,stat,priority,sched,psr,pcpu,pmem,time,cmd'
psax_mon() {
	t=$1
	[ -z "${t}" ] && t=2
	watch -n "${t}" "$(type psax | sed -e 's|.*`||' | sed -e "s|'||")"
}

# tools
alias backup_this_dir='time super_nice 7z a ../backup-$(basename $PWD).7z -mmt$(nproc --all) .'
alias dush='du -sh'
alias clear_tmpdir='rm -fr $TMPDIR/*'
alias apt_upgrade='apt update && apt upgrade'

# android / termux
alias android_top='top -o pid,user,s,pr,sched,psr,pcpu,virt,res,shr,%mem,time,cmdline -s 7'
alias proot_distro_symlink='ln -vsT ../usr/var/lib/proot-distro ./proot-distro'
termux_bootstrap_download_arch() {
	echo "curl -LOC- $(git config --get remote.origin.url)/releases/download/$(git tag | tail -n1)/bootstrap-${1}.zip"
	curl -LOC- "$(git config --get remote.origin.url)/releases/download/$(git tag | tail -n1)/bootstrap-${1}.zip"
}
alias termux_bootstrap_extract_usr='unzip -d usr'
alias termux_bootstrap_symlinks_txt_fixup_ln_for_sh='sed -e "s|\(.*\)←\(.*\)|ln -fsv \"\1\" \"\2\"|g" -i SYMLINKS.txt'
alias termux_bootstrap_symlinks_txt_xargs_split_1='sed -e "s|\(.*\)←\(.*\)|\"\1\" \"\2\"|g" -i SYMLINKS.txt'
alias termux_bootstrap_symlinks_txt_xargs_split_2='cat SYMLINKS.txt | xargs -P $(nproc) -L 1 ln -fsv'
alias termux_prepare_prefix_from_bootstrap_aarch64='
rm -fr usr;
cd termux-packages;
termux_bootstrap_extract_usr bootstrap-aarch64.zip;
cd usr;
termux_bootstrap_fixup_symlinks_txt_xargs_split_1;
termux_bootstrap_fixup_symlinks_txt_xargs_split_2;
rm -v SYMLINKS.txt;
cd ..;
mv -v usr ../usr;
cd ..
'
proot_prefix() {
	local p=$(realpath "${1}")
	echo "proot -b ${p}:${PREFIX} login"
	proot -b "${p}:${PREFIX}" login
}
alias termux_get_external_path='dirname $(dirname $(dirname $(dirname $(realpath $HOME/storage/external-1))))'
termux_renice_all() {
	for i in $(ps ax --no-headers -o pid); do
		renice -n 0 -p "$i"
	done
}
termux_extract_zip_tar() {
	unzip "$1"
	tar -xvf "${1/.zip/.tar}"
}
termux_packaging_download() {
	local TERMUX_PKG_SRCURL=$(. "$1"; echo $TERMUX_PKG_SRCURL)
	echo "$1 ... ${TERMUX_PKG_SRCURL}"
	curl -LOC - "${TERMUX_PKG_SRCURL}"
}
termux_packaging_git_clone() {
	local TERMUX_PKG_SRCURL=$(. "$1"; echo $TERMUX_PKG_SRCURL)
	echo "$1 ... ${TERMUX_PKG_SRCURL}"
	git clone --depth 1 --recursive "${TERMUX_PKG_SRCURL#git+}"
}
termux_packaging_check_conflict() {
	local package="$1"
	local contents=$(dpkg -L "${package}")
	local files=$(echo "${contents}" | xargs -P$(nproc) -i bash -c 'if [[ -f "{}" ]]; then echo "{}"; fi' | sort)
	for f in ${files}; do echo "$f: $(apt-file find $f | wc -l)"; done
}
termux_packaging_check_deps() {
	local package="$1"
	local contents=$(dpkg -L "${package}")
	local files=$(echo "${contents}" | xargs -P$(nproc) -i bash -c 'if [[ -f "{}" ]]; then echo "{}"; fi')
	local deps=$(echo "${files}" | xargs -P$(nproc) -n1 readelf -d 2>/dev/null | sed -ne "s|.*NEEDED.*\[\(.*\)\].*|\1|p" | sort | uniq)
	echo "${deps}"
	for dep in ${deps}; do apt-file -x find "/${dep}$"; done | sed -e "/^ndk-.*/d"
}

# python
alias python_activate_venv='. .venv/bin/activate; python --version'
alias python_pip_requirements='pip install -r requirements.txt'
alias python3_start_httpserver='ifconfig; python3 -m http.server'
alias python3_new_venv_unix='
python3 -m venv .venv;
python_activate_venv'
alias python2_new_venv_unix='
python3_new_venv_unix;
pip3 install virtualenv;
virtualenv -p $(command -v python2) .venv;
python_activate_venv'

# c / c++
list_compile_flags() {
	cat <<- EOL
	CFLAGS=$CFLAGS
	CXXFLAGS=$CXXFLAGS
	ASMFLAGS=$ASMFLAGS
	LDFLAGS=$LDFLAGS
	EOL
}
set_my_compile_flags_native(){
	case "$(uname -m)" in
	aarch64|arm)
	export CFLAGS="$CFLAGS -mcpu=native"
	export CXXFLAGS="$CXXFLAGS -mcpu=native"
	export ASMFLAGS="$ASMFLAGS -mcpu=native"
	;;
	x86_64|i686)
	export CFLAGS="$CFLAGS -march=native"
	export CXXFLAGS="$CXXFLAGS -march=native"
	export ASMFLAGS="$ASMFLAGS -march=native"
	;;
	esac
	list_compile_flags
}
set_my_compile_flags_O2() {
	export CFLAGS="$CFLAGS -O2"
	export CXXFLAGS="$CXXFLAGS -O2"
	export ASMFLAGS="$ASMFLAGS -O2"
	list_compile_flags
}
set_my_compile_flags_ldflags() {
	case "$(uname -o)" in
	Android) export LDFLAGS="$LDFLAGS -L/vendor/lib64" ;;
	esac
	list_compile_flags
}
alias unset_compile_flags='unset CFLAGS CXXFLAGS ASMFLAGS LDFLAGS'
taskset_cc_per_core() {
	count=0
	for tid in $(pgrep -w "c\+\+") \
				$(pgrep -w "g\+\+") \
				$(pgrep -w "cc") \
				$(pgrep -w "clang") \
				$(pgrep -w "llvm"); do
		taskset -acp "${count}" "${tid}"
		if [ "$count" != "$(($(nproc --all)-1))" ]; then
			count=$((count+1))
		fi
	done
}
taskset_cc_per_core_rev() {
	count=$(($(nproc --all)-1))
	for tid in $(pgrep -w "c\+\+") \
				$(pgrep -w "g\+\+") \
				$(pgrep -w "cc") \
				$(pgrep -w "clang") \
				$(pgrep -w "llvm"); do
		taskset -acp "${count}" "${tid}"
		if [ "$count" != 0 ]; then
			count=$((count-1))
		fi
	done
}
alias show_cc_cpu_usage="ps -o pcpu --no-headers -p $(pidof 'c++')"

# emscripten / emsdk
alias npm_ci_no_optional_production='npm ci --no-optional --production'
alias npm_ci_no_optional='npm ci --no-optional'
alias npm_install_no_optional_production='npm install --no-optional --production'
alias npm_install_no_optional='npm install --no-optional'
alias emsdk_activate='cd ~/emsdk; . ./emsdk_env.sh; cd -'
alias emsdk_clean_ffxd_all='
cd ~/emsdk && git clean -ffxd &
cd ~/emsdk/llvm/git/src && git clean -ffxd &
cd ~/emsdk/binaryen/main && git clean -ffxd &
cd ~/emsdk/emscripten/main && git clean -ffxd &
wait'
alias emsdk_git_pull_all='
cd ~/emsdk && git pull &
cd ~/emsdk/llvm/git/src && git pull &
cd ~/emsdk/binaryen/main && git pull &
cd ~/emsdk/emscripten/main && git pull &
wait'
alias emsdk_build_all='
cd ~/emsdk && git pull;
time super_nice ./emsdk install llvm-git-main-64bit --generator=Ninja -j4 &
time super_nice ./emsdk install binaryen-main-64bit --generator=Ninja -j4 &
time super_nice ./emsdk install emscripten-main-64bit &
wait;
cd -'
alias emsdk_generate_git_commit='
echo "emsdk:      $(cd ../.. && git rev-parse HEAD)" > commit.txt
echo "llvm:       $(cd ../../llvm/git/src && git rev-parse HEAD)" >> commit.txt;
echo "binaryen:   $(cd ../../binaryen/main && git rev-parse HEAD)" >> commit.txt;
echo "emscripten: $(cd ../../emscripten/main && git rev-parse HEAD)" >> commit.txt'

alias emtest_emcc_cores_1='export EMCC_CORES=1'
alias emtest_emcc_cores_4='export EMCC_CORES=4'
alias emtest_emcc_cores_6='export EMCC_CORES=6'
alias emtest_skip_v8='export EMTEST_SKIP_V8=1'
alias emtest_run_corefull='
time test/runner core* skip:core0.test_bigswitch 2>&1 | tee emtest_$(cat emscripten-version.txt)_core_0.txt'

alias emtest_run_core0_x2='
time test/runner core0 skip:core0.test_bigswitch 2>&1 | tee emtest_$(cat emscripten-version.txt)_core0_0.txt &&
time test/runner core0 skip:core0.test_bigswitch 2>&1 | tee emtest_$(cat emscripten-version.txt)_core0_1.txt'
alias emtest_run_core1_x2='
time test/runner core1 2>&1 | tee emtest_$(cat emscripten-version.txt)_core1_0.txt &&
time test/runner core1 2>&1 | tee emtest_$(cat emscripten-version.txt)_core1_1.txt'
alias emtest_run_core2_x2='
time test/runner core2 2>&1 | tee emtest_$(cat emscripten-version.txt)_core2_0.txt &&
time test/runner core2 2>&1 | tee emtest_$(cat emscripten-version.txt)_core2_1.txt'
alias emtest_run_core3_x2='
time test/runner core3 2>&1 | tee emtest_$(cat emscripten-version.txt)_core3_0.txt &&
time test/runner core3 2>&1 | tee emtest_$(cat emscripten-version.txt)_core3_1.txt'
alias emtest_run_cores_x2='
time test/runner cores 2>&1 | tee emtest_$(cat emscripten-version.txt)_cores_0.txt &&
time test/runner cores 2>&1 | tee emtest_$(cat emscripten-version.txt)_cores_1.txt'
alias emtest_run_corez_x2='
time test/runner corez 2>&1 | tee emtest_$(cat emscripten-version.txt)_corez_0.txt &&
time test/runner corez 2>&1 | tee emtest_$(cat emscripten-version.txt)_corez_1.txt'

alias emtest_run_other_x2='
time test/runner other 2>&1 | tee emtest_$(cat emscripten-version.txt)_other_0.txt &&
time test/runner other 2>&1 | tee emtest_$(cat emscripten-version.txt)_other_1.txt'

emcc_build_options='
-s MAX_WEBGL_VERSION=2
-s MIN_WEBGL_VERSION=2
-s ALLOW_MEMORY_GROWTH=1
-s USE_LIBPNG=1'

# boinc
boinc_use_all_cores() {
	for i in $(ps -o pid --no-headers -g $(pidof boinc) -H); do
		use_all_cores $i
	done
}
boinc_use_little_cores() {
	for i in $(ps -o pid --no-headers -g $(pidof boinc) -H); do
		use_little_cores $i
	done
}

# ffmpeg / audio / video
alias ffmprobe_get_audio_video_info_ffmprove='echo ffmprobe'
alias ffmpeg_extract_audio_from_video='echo "ffmpeg -i input.mp4 -vn -acodec copy audio.m4a"'
alias ffmpeg_decode_all_frames='echo "ffmpeg -i input.mp4 input_frames/frame_%06d.png"'
alias ffmpeg_interpolate_frame_with_audio='echo "ffmpeg -framerate 30 -i input_frames/frame_%06d.png -i audio.m4a -c:a copy -crf 20 -c:v libx264 -pix_fmt yuv420p output.mp4"'

# opencl
alias opencl_clinfo_clpeak='time ( cp -f clinfo.txt clinfo.old.txt; cp -f clpeak.txt clpeak.old.txt; clinfo | tee clinfo.txt; clpeak | tee clpeak.txt; diff --color=always -uN clinfo.old.txt clinfo.txt; diff --color=always -uN clpeak.old.txt clpeak.txt; : )'

# projects
alias find_orig_files='find . -name "*.orig"'
alias find_rej_files='find . -name "*.rej"'
alias find_orig_rej_files_clean='find . \( -name "*.orig" -o -name "*.rej" \) -exec rm -frv "{}" \;'
alias git_log_patch_with_stat='git log --patch-with-stat'
alias git_log_oneline_all_graph='git log --oneline --all --graph'
alias git_checkout_master_pull_all_prune_push_truboxl_master='git checkout master && git pull --all --prune && git push truboxl master'
alias git_status='git status'

# shortcuts
alias open_google_keep='am start --user 0 -n com.google.android.keep/com.google.android.keep.activities.BrowseActivity'

" .vimrc
" 4 spaces indent
set tabstop=4
set shiftwidth=4
set softtabstop=4
"set expandtab

" show line no
set number

" increase line memory
set viminfo='100,<1000,s100,h

" hard limits / no letter after line
set colorcolumn=50,72,80,100,120
highlight colorcolumn ctermbg=DarkGray guibg=DarkGray

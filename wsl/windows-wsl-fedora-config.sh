#!/bin/sh
set -e
# Get Fedora Remix for WSL from
# https://github.com/WhitewaterFoundry/Fedora-Remix-for-WSL

##### HIGH PRIORITY #####

. ../linux/linux-fedora-config-generic.sh

##### ADDON #####

SCRIPT="$(echo $0 | sed -E -e "s|$(dirname $0)+/||")"

if [ "$UID" = 0 ]; then
    echo "Running $SCRIPT as root!"
    unset SU
    echo
else
    echo "Not running $SCRIPT as root!"
    echo 'Prepending certain commands with sudo'
    SU=sudo
    echo
fi

echo 'Enabling 3rd party repo' # https://rpmfusion.org/Configuration
$SU dnf install -y \
https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
echo

echo "Done running $SCRIPT"

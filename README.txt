truboxl/scripts

Some personal scripts I use daily. See LICENSE.txt for license info for the scripts.

"3rdparty-" prefixed scripts are copied elsewhere from their respective sources indicated inside
the scripts and are not covered by LICENSE.txt.
